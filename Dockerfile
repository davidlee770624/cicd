# build
FROM maven:3-jdk-8-alpine as build
WORKDIR /build
COPY . /build
RUN mvn clean package -DskipTests

# service
FROM openjdk:8u312-oraclelinux8
WORKDIR /app
COPY --from=build /build/target/*.jar .
